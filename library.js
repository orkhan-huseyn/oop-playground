

class TodoApp {
  constructor(options = {}) {
    this.root = document.createElement('div');
    this.options = options;
    this.form = null;
    this.input = null;
    this.listContainer = null;

    this.handleDeleteItem = this.handleDeleteItem.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);

    this.createView();
    this.addEventListeners();
    this.applyCustomizations();
  }

  applyCustomizations() {
    const { containerClass, inputClass, listContainerClass } = this.options;
    this.root.className = containerClass || '';
    this.input.className = inputClass || '';
    this.listContainer.className = listContainerClass || '';
  }

  addEventListeners() {
    this.form.addEventListener('submit', this.handleFormSubmit);
    this.listContainer.addEventListener('click', this.handleDeleteItem);
  }

  removeEventListeners() {
    this.form.removeEventListener('submit', this.handleFormSubmit);
    this.listContainer.removeEventListener('click', this.handleDeleteItem);
  }

  createView() {
    this.createForm();
    this.createList();
  }

  createForm() {
    this.form = document.createElement('form');
    this.input = document.createElement('input');
    this.input.placeholder = 'What to do?...';
    this.input.style.width = '100%';
    this.form.append(this.input);
    this.root.append(this.form);
  }

  createList() {
    this.listContainer = document.createElement('ul');
    this.root.append(this.listContainer);
  }

  createListItem(textContent = '') {
    const listItem = document.createElement('li');
    listItem.className = this.options.listItemClass || '';

    const span = document.createElement('span');
    span.className = this.options.listItemTextClass || '';
    span.textContent = textContent;

    const button = document.createElement('button');
    button.dataset.action = 'delete';
    button.className = this.options.deleteButtonClass || '';
    button.innerHTML = '&times;';

    listItem.append(span);
    listItem.append(button);

    return listItem;
  }

  handleFormSubmit(event) {
    event.preventDefault();
    const text = this.input.value;
    const listItem = this.createListItem(text);
    this.listContainer.append(listItem);
    this.input.value = '';
  }

  handleDeleteItem(event) {
    if (event.target.matches('button[data-action="delete"]')) {
      event.target.parentElement.remove();
    }
  }

  mount(container) {
    if (!container) throw new Error('container is not specified.');
    if (!(container instanceof HTMLElement)) {
      throw new TypeError('container is not an instance of HTMLElement');
    }
    container.append(this.root);
  }

  unmount() {
    this.removeEventListeners();
    this.root.remove();
  }
}
