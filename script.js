
const root = document.getElementById('root');

const todoApp = new TodoApp({
  containerClass: 'w-1/2 mx-auto mt-3',
  inputClass: 'rounded-md border p-2',
  listContainerClass: 'mt-3',
  listItemClass: 'flex justify-between items-center w-full p-2 border rounded-sm mb-2',
  listItemTextClass: 'text-italic',
  deleteButtonClass: 'bg-red-600 text-white rounded-sm h-auto p-1',
});

todoApp.mount(root);
