//
// function Animal() {
//   this.canWalk = true;
//   this.name = undefined;
//   this.age = undefined;
// }
//
// Animal.gender = 'male';
// Animal.hello = function() {
//   console.log('hello static');
// };
//
// Animal.prototype.setName = function(name) {
//   this.name = name;
// };
//
// Animal.prototype.speak = function(name) {
//   console.log('Hi, my name is ' + this.name);
// };

class Animal {
  canWalk = true;
  name;
  #age = 12;

  get age() {
    console.log('Getter invoked!');
    return this.#age;
  }

  set age(age) {
    if (age < 18) {
      throw new Error('You cannot use this method!');
    }
    this.#age = age;
  }

  static gender = 'male';

  static hello() {
    console.log('hello static');
  }

  setName(name) {
    this.name = name;
  }

  speak() {
    console.log('Hi, my name is ' + this.name);
  }
}

const animal1 = new Animal();
animal1.setName('Filankes');

const animal2 = new Animal();
animal2.setName('Filankesov');

animal1.speak();
animal2.speak();


class Car {
  constructor() {
    this.model = '';
    this.year = 0;
    this.engine = 0;
  }

  drive() {
    console.log(`Driving ${this.model} from ${this.year}`);
  }
}


class Cheshka extends Car {

  constructor(year, engine) {
    super();
    this.model = 'Mercedes';
    this.year = year;
    this.engine = engine;
  }

  sur() {
    this.drive();
  }
}


const cheshka1 = new Cheshka(1998, 2.2);
const cheshka2 = new Cheshka(1996, 3.0);

cheshka1.sur();
cheshka2.sur();
